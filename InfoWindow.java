import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

/** 
* Klasa <code>InfoWindow</code> definiujaca
* okno zawieraj�ce informacje o autorze aplikacji
* @author Bart�omiej Lewandowski
*/
public class InfoWindow extends JDialog implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JLabel l0, l1, l2, l3, l4, l5;
	private JLabel lBorder;
	private JButton ok;
	private Font font1 = new Font("TimesRoman", Font.BOLD, 22);
	private Font font3 = new Font("TimesRoman", Font.BOLD, 12);
	private Font font4 = new Font("TimesRoman", Font.PLAIN, 12);
	private Border line = null;
	
	
	/**
	 * Definicja konstruktora domy�lnego klasy
	 */
	public InfoWindow() {
		this.setTitle("Informacje o programie");
		this.setModal(true);
		this.setSize(380,250);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		
		//Definicja operacji zamkni�cia okna
		this.addWindowListener	(new WindowAdapter(){ 
			public void windowClosing(WindowEvent e){ 
				setVisible(false);				
			}	
		});	
		
		
		Container contentPane = getContentPane();
		contentPane.setBackground(Color.white);
		contentPane.setLayout(null);
		
		
		l0 = new JLabel("Moja aplikacja");
		l0.setFont(font1);
		l0.setHorizontalAlignment(SwingConstants.CENTER);
		l1 = new JLabel("Wersja 1.0");
		l1.setFont(font1);
		l1.setHorizontalAlignment(SwingConstants.CENTER);
		l2 = new JLabel("Copyright (C) by 2020");
		l2.setFont(font4);
		l2.setHorizontalAlignment(SwingConstants.CENTER);
		l3 = new JLabel("Bart�omiej Lewandowski");
		l3.setFont(font3);
		l3.setHorizontalAlignment(SwingConstants.CENTER);
		l4 = new JLabel("Politechnika Koszalinska - WEiI");
		l4.setFont(font3);
		l4.setHorizontalAlignment(SwingConstants.CENTER);
		l5 = new JLabel("lewandowski.bartlomiej@wp.pl");
		l5.setFont(font4);
		l5.setHorizontalAlignment(SwingConstants.LEFT);
		lBorder = new JLabel(""); 
		ok = new JButton("Ok");
		ok.addActionListener(this);
		line = new EtchedBorder(EtchedBorder.LOWERED);
		
		l0.setBounds(75,20,210,30);
		l1.setBounds(75,50,210,30);
		l2.setBounds(75,100, 210,20);
		l3.setBounds(75,120,210,20);
		l4.setBounds(75,140,210,20);
		lBorder.setBounds(5,175,340,2);
		l5.setBounds(10,182,200,20);
		ok.setBounds(290,182,60,25);
		
		lBorder.setBorder(line);
		contentPane.add(l0);
		contentPane.add(l1);
		contentPane.add(l2);
		contentPane.add(l3);
		contentPane.add(l4);
		contentPane.add(l5);
		contentPane.add(lBorder);
		contentPane.add(ok);
	}
	
	/**
	 * Metoda obs�ugujaca zdarzenie akcji
	 */
	@Override
	public void actionPerformed(ActionEvent eAE) {
		if(eAE.getSource() == ok) {
			setVisible(false);
		}
	}
}