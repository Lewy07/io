import java.util.Vector;
import javax.swing.*;

/** 
* Klasa <code>ListModel</code> definiujaca
* rozszerzająca model listy
* @author Bartłomiej Lewandowski
*/
class ListModel extends AbstractListModel<String> {

	private static final long serialVersionUID = 1L;
	Vector<Object> v = new Vector<Object>();
	
	public ListModel() { }
	
	public ListModel(Object[] objects) 
	{
		for(int i = 0; i < objects.length; i++) 
		{
			v.addElement(objects[i]);
		}
	}
	
	public String getElementAt(int index) { return (String) v.elementAt(index); }
	public int getSize() { return v.size(); }
	public void add(Object object) {
		add(getSize(),object);
	}
	public void add(int index, Object object) {
		v.insertElementAt(object, index);
		fireIntervalAdded(this,index,index);
	}
	
	public void remove(int index) {
		v.removeElementAt(index);
		fireIntervalRemoved(this, index, index);
	}
}