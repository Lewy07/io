import java.awt.BorderLayout;
import java.awt.event.*;
import javax.swing.*;

/** 
* Klasa <code>Window</code> definiujaca
* g��wne okno aplikacji
* @author Bart�omiej Lewandowski
*/
public class Window extends JFrame implements ActionListener,Runnable
{
	/**
	 * Zmienne tworz�ce g��wne okno aplikacji.
	 */
	private static final long serialVersionUID = 1L;
	private static final int HEIGHT = 175;
	private static final int WIDTH = 420;
	private Center centerPanel = new Center();
	private InfoWindow info = new InfoWindow();
	private JPanel mainPane;
	/**
	 * Zmienne tworz�ce menu aplikacji.
	 */
	private JMenuBar menuBar;
	private JMenu fileMenu, editMenu, viewMenu, actionMenu, helpMenu;
	private JMenuItem helpAboutMenuItem;
	
	
	/**
	 * Definicja konstruktora domy�lnego klasy <CODE>Window<CODE>
	 */
	public Window()
	{
		this.setTitle("Moja aplikacja");
		
		this.setSize(WIDTH, HEIGHT);
		this.setLocationRelativeTo(null);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		createMenu();
		
		mainPane = (JPanel) this.getContentPane();
		mainPane.setLayout(new BorderLayout());
		
		try {	
	        this.run();	
		}
		catch(Exception e) {
			System.out.println("B��d podczas uruchamiania!");
			JOptionPane.showMessageDialog(null, "B��d podczas uruchamiania!", "B��d", JOptionPane.ERROR_MESSAGE);
		}

		
	   Thread thread = new Thread(this);
	   thread.start();
	}
		
	
	/**
	 * Metoda tworz�ca menu. 
	 */
	public void createMenu()
	{
		menuBar = new JMenuBar();
		
		fileMenu = new JMenu("Plik");
		editMenu = new JMenu("Edycja");
		viewMenu = new JMenu("Widok");
		actionMenu = new JMenu("Obliczenia");
		helpMenu = new JMenu("Pomoc");
				
		helpAboutMenuItem = new JMenuItem("Informacja");
		helpAboutMenuItem.addActionListener(this);

		helpMenu.add(helpAboutMenuItem);
		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(viewMenu);
		menuBar.add(actionMenu);
		menuBar.add(helpMenu);
		this.setJMenuBar(menuBar);
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == helpAboutMenuItem){
		 	info.setVisible(true);
		}
	}
	
	@Override
	public void run() {
		mainPane.add(centerPanel, BorderLayout.CENTER);
		centerPanel.setVisible(true);
	}
	
	public static void main(String args[])
	{
		Window test = new Window();
		test.setVisible(true);
	}
}
