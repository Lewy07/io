import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;

import javax.swing.*;
import javax.swing.border.*;

/** 
* Klasa <code>Center</code> definiujaca
* panel centralny aplikacji
* oraz g�owne funkcjonalno�ci
* @author Bart�omiej Lewandowski
*/
public class Center extends JPanel implements ActionListener {

	private static final long serialVersionUID = 1L;

	/**
	 * Zmienne sk�adaj�ce si� na panel centralny aplikacji, wykorzystywane do realizacji podstawowych zagadnie�
	 */
	private TitledBorder titledBorder;
	private Border blackLine;
	private JTextField value;
	private JLabel info, currency_zl, result;
	private JButton doButton;
	private float operation_result = 0;
	private DecimalFormat dec = new DecimalFormat("#0.00");
	/**
	 * Zmienne tworz�ce Liste zawieraj�c� podstawowe waluty.
	 */
	private String[] str = { "GBP", "JPY", "EURO", "USD"};
	private ListModel modelList;
	private JList<String> list;

	
	/**
	 * Definicja konstruktora domy�lnego klasy <CODE>Center<CODE>
	 */
	public Center() 
	{
		blackLine = BorderFactory.createLineBorder(Color.gray);
		titledBorder = BorderFactory.createTitledBorder(blackLine, "Konwerter walut");
		titledBorder.setTitleJustification(TitledBorder.CENTER);
		this.setBorder(titledBorder);
		this.setLayout(new FlowLayout());
		
		modelList = new ListModel(str);
		list = new JList<String>(modelList);
		list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setFixedCellHeight(12);		
		JScrollPane jsp = new JScrollPane(list);
		jsp.setPreferredSize(new Dimension(60, 54));
		
		info = new JLabel("Wpisz warto��:");
		currency_zl = new JLabel("z�");
		result = new JLabel("0,00 GBP");
		
		value = new JTextField(10);
		value.setText("0");
		value.setHorizontalAlignment(JTextField.RIGHT);
		
		doButton = new JButton("Wykonaj");
		doButton.addActionListener(this);
		
		this.add(info);
		this.add(value);
		this.add(currency_zl);
		this.add(jsp);
		this.add(result);
		this.add(doButton);
	}
	
					
	/**
	 * Metoda obs�ugujaca zdarzenia akcji
	 */
	@Override
	public void actionPerformed(ActionEvent e) 
	{
		if(e.getSource() == doButton) 
		{			
			try 
			{
				operation_result = Float.parseFloat(value.getText().trim());
						
				if(operation_result < 0)
				{
					JOptionPane.showMessageDialog(null, "Warto�� musi by� wi�ksza od 0!", "B��d", JOptionPane.ERROR_MESSAGE);
				}
				else 
				{
					String s = (String) list.getSelectedValue();
					if(s.equals("GBP"))
					{
						operation_result = operation_result / 5.1f;
						result.setText(dec.format(operation_result)+" GBP");
					}
					else if(s.equals("USD"))
					{
						operation_result = operation_result / 3.95f;
						result.setText(dec.format(operation_result)+" USD");
					}
					else if(s.equals("EURO"))
					{
						operation_result = operation_result / 4.6f;
						result.setText(dec.format(operation_result)+" EURO");
					}
					else if(s.equals("JPY"))
					{
						operation_result = operation_result * 26.53f;
						result.setText(dec.format(operation_result)+" JPY");
					}	
				}
			}
			catch (Exception z) 
			{
				JOptionPane.showMessageDialog(null, "Wprowadzono b��dn� warto��!", "B��d", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}